﻿using System;
using System.Collections.Generic;

namespace BinaryTree
{
	public class BinarySearchTree<T> : IBinarySearchable<BinaryTreeNode<T>, T> where T : IComparable<T>
	{
		private BinaryTreeNode<T> baseNode;
		public BinarySearchTree()
		{
			baseNode = null;
		}

		public BinarySearchTree(T value)
		{
			baseNode = new BinaryTreeNode<T>(value);
		}

		public BinarySearchTree(T value, BinaryTreeNode<T> parent)
		{
			baseNode = new BinaryTreeNode<T>(value, parent);
		}

		public void deleteAllChildren()
		{
			baseNode.leftTree = null;
			baseNode.rightTree = null;
		}

		public void deleteAll()
		{
			baseNode = null;
		}

		/*
		 * @return: A BinaryTreeNode reference or null if value is not found in tree
		 * @param value - The value to search for in the tree, which must implement IComparable<>
		 * @Time Complexity: O(lgN) for a balanced tree, O(n) in the worst case
		 */
		public BinaryTreeNode<T> retrieve(T value)
		{

			BinaryTreeNode<T> curNode = this.baseNode;
			while (curNode != null)
			{
				if (curNode.Value.CompareTo(value) < 0)
				{
					curNode = curNode.rightTree;
				}
				else if(curNode.Value.CompareTo(value) > 0)
				{
					curNode = curNode.leftTree;
				}
				else
                {
					return curNode;
                }
			}

			return curNode; // base node was null, i.e. empty tree or no match found

		}

		public bool delete(T value) //follows Hibbards algorithm more or less
		{
			bool didDelete = false;
			BinaryTreeNode<T> deleteNode = retrieve(value);
			if (deleteNode != null)
			{
				if (deleteNode.leftTree == null && deleteNode.rightTree == null)
				{
					if (deleteNode == this.baseNode)
					{
						this.baseNode = null;
						didDelete = true;
					}
					else if (deleteNode.parentTree.rightTree == deleteNode)
					{
						deleteNode.parentTree.rightTree = null;
						didDelete = true;
					}
					else if (deleteNode.parentTree.leftTree == deleteNode)
					{
						deleteNode.parentTree.leftTree = null;
						didDelete = true;
					}
				}
				else if (deleteNode.leftTree == null) //right node is non-null
				{
					if (deleteNode == this.baseNode) //deleting root with no left subtree
					{
						this.baseNode = this.baseNode.rightTree;
						didDelete = true;
					}
					else if (deleteNode.parentTree.rightTree == deleteNode)
					{
						deleteNode.parentTree.rightTree = deleteNode.rightTree;
						didDelete = true;
					}
					else if (deleteNode.parentTree.leftTree == deleteNode)
					{
						deleteNode.parentTree.leftTree = deleteNode.rightTree;
						didDelete = true;
					}
				}
				else if (deleteNode.rightTree == null) //left node is non-null
				{
					if (deleteNode == this.baseNode) //deleting root with no right subtree
					{
						this.baseNode = this.baseNode.leftTree;
						didDelete = true;
					}
					else if (deleteNode.parentTree.rightTree == deleteNode)
					{
						deleteNode.parentTree.rightTree = deleteNode.leftTree;
						didDelete = true;
					}
					else if (deleteNode.parentTree.leftTree == deleteNode)
					{
						deleteNode.parentTree.leftTree = deleteNode.leftTree;
						didDelete = true;
					}
				}
				else //both children of deleteNode are non-null
				{
					if (deleteNode.rightTree.leftTree == null) //right node of delete node is successor, i.e. right node has no left subtree
					{
						deleteNode.Value = deleteNode.rightTree.Value;
						deleteNode.rightTree = deleteNode.rightTree.rightTree;
						didDelete = true;
					}
					else //right node of delete node has left subtree
					{
						BinaryTreeNode<T> rightMin = deleteNode.rightTree, rightMinParent, rightMinRight;
						while (rightMin.leftTree != null)
						{
							rightMin = rightMin.leftTree;
						}

						rightMinParent = rightMin.parentTree;
						rightMinRight = rightMin.rightTree;

						deleteNode.Value = rightMin.Value;
						rightMinParent.leftTree = rightMinRight;
						didDelete = true;
					}
				}

			}

			return didDelete;
		}

		public IEnumerable<BinaryTreeNode<T>> inorderIterator
		{
			get
			{
				if (baseNode == null)
				{
					yield break;
				}

				Stack<BinaryTreeNode<T>> treeNodes = new Stack<BinaryTreeNode<T>>();
				BinaryTreeNode<T> temp = baseNode;
				treeNodes.Push(temp);
				while (treeNodes.Count > 0)
				{
					while (temp.leftTree != null)
					{
						temp = temp.leftTree;
						treeNodes.Push(temp);

					}

					temp = treeNodes.Pop();
					yield return temp;
					while (temp.rightTree == null && treeNodes.Count > 0)
					{
						temp = treeNodes.Pop();
						yield return temp;

					}
					if (temp.rightTree != null) //node may have right subtree
					{
						temp = temp.rightTree;
						treeNodes.Push(temp);
					}
				}
			}
		}

		/* @return - An array of type T which contains all values in this tree in order as defined by T.compareTo()
		 */
		public T[] toArray()
		{
			List<T> inorderNodes = new List<T>();
			IEnumerable<BinaryTreeNode<T>> nodeIter = inorderIterator;
			foreach (BinaryTreeNode<T> temp in nodeIter)
				inorderNodes.Add(temp.Value);
			return inorderNodes.ToArray();
		}

		public void printInorder()
		{
			if (baseNode == null)
			{
				return;
			}

			Stack<BinaryTreeNode<T>> treeNodes = new Stack<BinaryTreeNode<T>>();
			BinaryTreeNode<T> temp = baseNode;
			treeNodes.Push(temp);
			while (treeNodes.Count > 0)
			{
				while (temp.leftTree != null)
				{
					temp = temp.leftTree;
					treeNodes.Push(temp);

				}

				temp = treeNodes.Pop();
				Console.WriteLine(temp.Value);
				while (temp.rightTree == null && treeNodes.Count > 0)
				{
					temp = treeNodes.Pop();
					Console.WriteLine(temp.Value);

				}
				if(temp.rightTree != null) //node may have right subtree
				{
					temp = temp.rightTree;
					treeNodes.Push(temp);
				}
			}
		}


		/*
		 * Inserts the value specified if it does not already exist in the tree
		 * This Tree (method) does not allow the insertion of duplicate elements
		 * @param Value - The value to be inserted
		 * @exceptions thrown: InvalidOperationException on attempting to insert a duplicate node into this tree. A duplicate node is one which has the same value as an existing node
		 */
		public void insert(T value)
		{
			bool inserted = false;
			if(this.baseNode == null)
			{
				this.baseNode = new BinaryTreeNode<T>(value);
				return;
			}

			if(this.retrieve(value) != null) // ensure no duplicates are inserted
			{
				throw new InvalidOperationException("Cannot insert duplicate value \"" + value + "\" into tree: " + this.ToString());
			}

			BinaryTreeNode<T> temp = baseNode;
			while(!inserted)
			{
				if(value.CompareTo(temp.Value) < 0)
				{
					if (temp.leftTree == null)
					{
						temp.leftTree = new BinaryTreeNode<T>(value, temp);
						inserted = true;
					}
					else
					{
						temp = temp.leftTree;
					}
				}
				else
				{
					if(temp.rightTree == null)
					{
						temp.rightTree = new BinaryTreeNode<T>(value, temp);
						inserted = true;
					}
					else
					{
						temp = temp.rightTree;
					}
				}
			}

		}

		public BinaryTreeNode<T> BinarySearch(T value)
		{
			return retrieve(value);
		}
	}
}
