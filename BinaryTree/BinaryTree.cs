﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryTree
{
    class BinaryTree<T> where T: IComparable<T>
    {
        private BinaryTreeNode<T> root;

        public BinaryTree(T value)
        {
            this.root = new BinaryTreeNode<T>(value);
        }
        
        public BinaryTree()
        {
            root = null;
        }

        public T getRootValue()
        {
            return this.root.Value;
        }

        public BinaryTreeNode<T> retrieve(T value)
        {
            if(this.root == null)
            {
                return null;
            }
            else
            {
                Queue<BinaryTreeNode<T>> nodeQueue = new Queue<BinaryTreeNode<T>>();
                nodeQueue.Enqueue(root);

                while (nodeQueue.Count > 0)
                {
                    BinaryTreeNode<T> tempNode = nodeQueue.Dequeue();

                    if(tempNode.Value.CompareTo(value) == 0)
                    {
                        return tempNode;
                    }
                    if (tempNode.leftTree != null)
                    {
                        nodeQueue.Enqueue(tempNode.leftTree);
                    }

                    if (tempNode.rightTree != null)
                    {
                        nodeQueue.Enqueue(tempNode.rightTree);
                    }

                }

                return null;
            }
        }

        public void deleteAll()
        {
            root = null;
        }

        public void delete(T value)
        {

            if (root == null)
                return;

            BinaryTreeNode<T> rightMostLeaf = root, deleteNode;
            while (!(rightMostLeaf.leftTree == null && rightMostLeaf.rightTree == null))
            {
                if(rightMostLeaf != null)
                    rightMostLeaf = rightMostLeaf.rightTree;
            }

            deleteNode = retrieve(value);
            if (deleteNode != null && rightMostLeaf != null)
            {
                deleteNode.Value = rightMostLeaf.Value;
                if(rightMostLeaf.parentTree != null)
                rightMostLeaf.parentTree.rightTree = null;
            }
        }

        public void printLevelOrder()
        {
            if(root == null)
            {
                return;
            }
            else
            {
                Queue<BinaryTreeNode<T>> nodeQueue = new Queue<BinaryTreeNode<T>>();
                nodeQueue.Enqueue(root);

                while (nodeQueue.Count > 0)
                {
                    BinaryTreeNode<T> tempNode = nodeQueue.Dequeue();
                    Console.WriteLine(tempNode.Value);
                    if (tempNode.leftTree != null)
                    {
                        nodeQueue.Enqueue(tempNode.leftTree);
                    }

                    if (tempNode.rightTree != null)
                    {
                        nodeQueue.Enqueue(tempNode.rightTree);
                    }

                }
            }
        }
        public void insertLevelOrder(T value)
        {

            if (root == null)
            {
                root = new BinaryTreeNode<T>(value);
            }
            else
            {
                Queue<BinaryTreeNode<T>> nodeQueue = new Queue<BinaryTreeNode<T>>();
                nodeQueue.Enqueue(root);

                while (nodeQueue.Count > 0)
                {
                    BinaryTreeNode<T> tempNode = nodeQueue.Dequeue();
                    if (tempNode.leftTree != null)
                    {
                        nodeQueue.Enqueue(tempNode.leftTree);
                    }
                    else
                    {
                        tempNode.leftTree = new BinaryTreeNode<T>(value, tempNode);
                        break;
                    }

                    if (tempNode.rightTree != null)
                    {
                        nodeQueue.Enqueue(tempNode.rightTree);
                    }
                    else
                    {
                        tempNode.rightTree = new BinaryTreeNode<T>(value, tempNode);
                        break;
                    }

                }
            }
        }
    }
}
