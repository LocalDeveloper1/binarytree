﻿using System;
using BinaryTree;
using System.Collections.Generic;

class BinaryTreeDriver
{
    static void Main(string[] args)
    {
        Random rand = new Random();
        BinaryTree<int> binTree = new BinaryTree<int>();

        for(int i = 0; i <= 100; i++) 
            binTree.insertLevelOrder(i);

        binTree.printLevelOrder();
        binTree.delete(45);
        binTree.printLevelOrder();
        binTree.deleteAll();
        binTree.delete(4);
        Console.WriteLine();


        for(int i = 0; i <= 100; i++) 
        {
            binTree.insertLevelOrder(rand.Next());
        }
        binTree.printLevelOrder();

        BinarySearchTree<int> binTree2 = new BinarySearchTree<int>();
        binTree2.insert(25);
        binTree2.insert(22);
        binTree2.insert(23);
        binTree2.insert(21);
        binTree2.insert(45);
        binTree2.insert(43);
        binTree2.insert(55);
        binTree2.insert(54);
        binTree2.insert(56);

        Console.Write("Before Delete:\n");
        binTree2.printInorder();
        binTree2.delete(21);
        Console.Write("\n\nAfter Delete:\n");
        binTree2.printInorder();

        Console.WriteLine("\n\n");
        int[] inorderInts = binTree2.toArray();
        int sum = 0;
        for(int i = 0; i < inorderInts.Length; i++)
        {
            sum += inorderInts[i];
        }

        Console.WriteLine("\n\nThe sum of the tree's ints is: " + sum);
        binTree2 = null;

        BinarySearchTree<string> binTree3 = new BinarySearchTree<string>();
        binTree3.insert("Kathy");
        binTree3.insert("Mike");
        binTree3.insert("Matt");
        binTree3.insert("Amanda");
        binTree3.insert("Marcus");
        binTree3.insert("Matt2");
        binTree3.printInorder();
        Console.WriteLine(binTree3.delete("Jim"));
        Console.WriteLine(binTree3.delete("Matt2"));

        IBinarySearchable<BinaryTreeNode<string>, string> a = binTree3;
        Console.WriteLine(a.BinarySearch("Marcus"));

        foreach(BinaryTreeNode<string> temp in binTree3.inorderIterator)
        {
            Console.WriteLine(temp);
        }


        /*int j = 0 ;
        for(int i = 0; i <= 20000; i++)
        {
            j = rand.Next();
            binTree2.insert(j);
        }

        Console.WriteLine(binTree2.retrieve(j) + "\n\n\n");
        binTree2.printInorder(); */
        /* try
        {
            Console.WriteLine(binTree.retrieve(100).Value);
        }
        catch(NullReferenceException r)
        {
            Console.Error.WriteLine(r.Message);
        } */
    }
}
