﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryTree
{
    public class BinaryTreeNode<T>
    {
        public T Value
        {
            get;
            internal set;
        }


        public BinaryTreeNode<T> leftTree
        {
            get;
            internal set;
        }

        public BinaryTreeNode<T> rightTree
        {
            get;
            internal set;
        }

        public BinaryTreeNode<T> parentTree
        {
            get;
            internal set;
        }

        public BinaryTreeNode(T value)
        {
            this.Value = value;
            this.parentTree = null;
        }

        public BinaryTreeNode(T value, BinaryTreeNode<T> parent)
        {
            this.Value = value;
            this.parentTree = parent;
        }

        public override string ToString()
        {
            return this.Value.ToString();
        }

    }
}
