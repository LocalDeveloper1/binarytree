﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryTree
{
    /*
     * Type Parameters: 
     *    E - The return type of this interfaces method
     *    T - The type of the value which populates the homogenous data structure which implements this interface
     */
    public interface IBinarySearchable<E,T> where T : IComparable<T>
    {
        /*
         * Purpose: This method will return an item of a collection or data structure of type E which contains
         *           a value of type T equal to the parameter passed.
         * Guarantees: This method shall have a worst case time complexity of O(N) and should 
         *              implement this using some form of binary search. It is encouraged to 
         *              provide an implementation which has a worst case time complexity of O(lgN) 
         *              and at least an average case time complexity of O(lgN).
         */
        public E BinarySearch(T value);

    }
}
