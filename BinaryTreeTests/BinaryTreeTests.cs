using System;
using Xunit;
using BinaryTree;

namespace BinaryTreeTests
{
    
    public class BinaryTreeTests
    {
        [Fact]
        public void retrieve_validItem_ReturnsNonNull()
        {
            //arrange
            BinarySearchTree<int> tree = new BinarySearchTree<int>();
            for (int i = 1; i <= 100; i++)
                tree.insert(i);

            //act
            BinaryTreeNode<int> node = tree.retrieve(55);

            //assert
            Assert.NotNull(node);
            Assert.Equal<int>(55, node.Value);

        }

        [Fact]
        public void retrieve_invalidItem_returnsNull()
        {
            //arrange
            BinarySearchTree<int> tree = new BinarySearchTree<int>();
            for (int i = 1; i <= 100; i++)
                tree.insert(i);

            //act
            BinaryTreeNode<int> node = tree.retrieve(101);

            //assert
            Assert.Null(node);
        }

        [Fact]
        public void retrieve_nondegenerateTreeValidItem_returnsNonNull()
        {
            //arrange
            BinarySearchTree<int> tree = new BinarySearchTree<int>(50);
            const int retrieveValue = 75;
            for (int i = 1; i <= 100; i++)
            {
                if(i != 50) //insert doesnt allow duplicates in this implementation
                    tree.insert(i);
            }

            //act
            BinaryTreeNode<int> node = tree.retrieve(retrieveValue);

            //assert
            Assert.NotNull(node);
            Assert.Equal<int>(retrieveValue, node.Value);
        }

        [Fact]
        public void retrieve_degenerateTreeInvalidItem_returnsNull()
        {
            //arrange
            BinarySearchTree<int> tree = new BinarySearchTree<int>(50);
            for (int i = 1; i <= 100; i++)
            {
                if (i != 50) //insert doesnt allow duplicates in this implementation
                    tree.insert(i);
            }

            //act
            BinaryTreeNode<int> node = tree.retrieve(101);

            //assert
            Assert.Null(node);
        }

        [Fact]
        public void retrieve_singleElementTreeNoMatch_returnsNull()
        {
            //arrange
            BinarySearchTree<int> tree = new BinarySearchTree<int>(50);

            //act
            BinaryTreeNode<int> node = tree.retrieve(101);

            //assert
            Assert.Null(node);
        }
    }
}
