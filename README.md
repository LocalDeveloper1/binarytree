# README #

* A repository containing a small project to create a binary tree data structure and variations in C#.  
* This code is not guaranteed to be defect free or suitable for any/every purpose.  
* I give the rights to others to use this code in personal projects for non-monetary gain or as a reference for their own works.  